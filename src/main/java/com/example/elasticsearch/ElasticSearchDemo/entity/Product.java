package com.example.elasticsearch.ElasticSearchDemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(indexName = "products")
public class Product {

    private String name;
    private String description;
    private int quantity;
    private double price;
    private int id;

}
