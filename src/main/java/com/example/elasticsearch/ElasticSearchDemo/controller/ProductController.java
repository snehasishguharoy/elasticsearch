package com.example.elasticsearch.ElasticSearchDemo.controller;

import com.example.elasticsearch.ElasticSearchDemo.entity.Product;
import com.example.elasticsearch.ElasticSearchDemo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductService productService;



    @GetMapping("/findAllProducts")
    public ResponseEntity<List<Product>> fetchAllProducts() {
        return ResponseEntity.ok(productService.getProducts());
    }


}
