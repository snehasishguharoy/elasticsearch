package com.example.elasticsearch.ElasticSearchDemo.repo;

import com.example.elasticsearch.ElasticSearchDemo.entity.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


public interface ProductRepo extends ElasticsearchRepository<Product, Integer> {
}
