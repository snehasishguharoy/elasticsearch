package com.example.elasticsearch.ElasticSearchDemo.service;

import com.example.elasticsearch.ElasticSearchDemo.entity.Product;
import com.example.elasticsearch.ElasticSearchDemo.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class  ProductService {

    @Autowired
    private ProductRepo productRepo;




    public List<Product> getProducts() {
        List<Product> list = new ArrayList<>();
        productRepo.findAll().forEach(list::add);
        return list;
    }

    public Product insertProduct(Product product) {
        return productRepo.save(product);
    }

    public Product updateProduct(Product product, int productId) {
        Product response = productRepo.findById(productId).get();
        response.setPrice(product.getPrice());
        return productRepo.save(response);
    }

    public void deleteProduct(int productId) {
        productRepo.deleteById(productId);

    }


}
